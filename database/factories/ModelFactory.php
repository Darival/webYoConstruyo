<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Module::class, function (Faker\Generator $faker) {
    return [
        'title'  => $faker->name,
    ];
});

$factory->define(App\SubModule::class, function (Faker\Generator $faker) {
    return [
        'index'     =>  $faker->randomDigit,
        'title'     =>  $faker->name,
        'content'   =>  $faker->text,
        'module_id' =>  function(){return factory(App\Models\Module::class)->create()->id;},
    ];
});

$factory->define(Backpack\PermissionManager\app\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});
