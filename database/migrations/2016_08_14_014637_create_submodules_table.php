<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmodulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('content');
            $table->string('title');
            $table->integer('index')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->foreign('module_id')
                    ->references('id')->on('modules')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_modules');
    }
}
