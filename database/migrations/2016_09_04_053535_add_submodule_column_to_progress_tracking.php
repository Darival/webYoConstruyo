<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmoduleColumnToProgressTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('progress_trackings', function(Blueprint $table){
            $table->integer('sub_module_id')->unsigned();
            $table->foreign('sub_module_id')->references('id')->on('sub_modules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('progress_trackings', function(Blueprint $table){
            $table->dropColumn('sub_module_id');
        });
    }
}
