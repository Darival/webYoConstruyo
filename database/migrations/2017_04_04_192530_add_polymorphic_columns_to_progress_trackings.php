<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPolymorphicColumnsToProgressTrackings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('progress_trackings', function (Blueprint $table) {
            $table->dropForeign('progress_trackings_sub_module_id_foreign');
            $table->dropColumn('sub_module_id');
            $table->string('progress_trackeable_type')->after('completed');
            $table->integer('progress_trackeable_id')->after('completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('progress_trackings', function (Blueprint $table) {
            $table->integer('sub_module_id')->unsigned()->after('completed');
            $table->foreign('sub_module_id')->references('id')->on('sub_modules')->onDelete('cascade');
            $table->dropColumn('progress_trackeable_type');
            $table->dropColumn('progress_trackeable_id');
            //
        });
    }
}
