<?php

use Illuminate\Database\Seeder;

class SubModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\SubModule::class)->create([
            'index'     =>  1,
            'title'     =>  'Presentación',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  2,
            'title'     =>  'Sostenibilidad',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  3,
            'title'     =>  'Criterios del diseño',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  4,
            'title'     =>  'Carácteristicas del terreno',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  5,
            'title'     =>  'Evolución de la vivienda',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  6,
            'title'     =>  'Condiciones climáticas',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  7,
            'title'     =>  'Procesos de construcción sostenible',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  8,
            'title'     =>  'Uso del Concreto',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  9,
            'title'     =>  'Agregados',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  10,
            'title'     =>  'Mezclas de Cemento',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  11,
            'title'     =>  'Tablas de proporciones para Morteros y Mezclas de Concreto',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  12,
            'title'     =>  'Armados',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
        
        factory(App\SubModule::class)->create([
            'index'     =>  13,
            'title'     =>  'Procedimientos Previos',
            'content'   =>  '',
            'module_id' =>  3,
        ]);
    }
}
