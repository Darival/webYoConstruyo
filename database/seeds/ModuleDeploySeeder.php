<?php

use Illuminate\Database\Seeder;

class ModuleDeploySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Module::class)->create([
            'title'     =>  'Introducción',
            'index'     =>  1,
            'thumbnail' =>  'img/parallax1.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Albañilerías',
            'index'     =>  2,
            'thumbnail' =>  'img/albanileria.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Cimentaciones',
            'index'     =>  3,
            'thumbnail' =>  'img/cimentacion.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Muros',
            'index'     =>  4,
            'thumbnail' =>  'img/muros.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Cerramientos y Vanos',
            'index'     =>  5,
            'thumbnail' =>  'img/cerramientos.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Losas',
            'index'     =>  6,
            'thumbnail' =>  'img/losas.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Azoteas',
            'index'     =>  7,
            'thumbnail' =>  'img/azotea.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Acabados',
            'index'     =>  8,
            'thumbnail' =>  'img/acabados.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Instalaciones hidrosanitarias',
            'index'     =>  9,
            'thumbnail' =>  'img/hidro.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Instalaciones Eléctricas',
            'index'     =>  10,
            'thumbnail' =>  'img/electricas.png'
        ]);
        factory(App\Models\Module::class)->create([
            'title'     =>  'Ecotecnias',
            'index'     =>  11,
            'thumbnail' =>  'img/pag120.png'
        ]);
    }
}
