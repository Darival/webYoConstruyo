<?php

use Illuminate\Database\Seeder;

class AdminDeploySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class)->create([
            'name'      =>  'Darival',
            'email'     =>  'yoadmin@yoconstruyo.com',
            'password'  =>  bcrypt('admin123')
        ]);
        $role = factory(Backpack\PermissionManager\app\Models\Role::class)->create([
            'name'      =>  'Admin',
        ]);

        $user->assignRole($role->name);

        $user = factory(App\User::class)->create([
            'name'      =>  'Darival',
            'email'     =>  'cesarrcv1992@hotmail.com',
            'password'  =>  bcrypt('Solid192!')
        ]);

        $user->assignRole($role->name);
    }
}
