<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProgressTest extends TestCase
{
	use DatabaseTransactions;

	public function setUp(){
		parent::setUp();


        $this->signIn();
	}

    /** @test */
    public function if_the_user_can_make_progress()
    {
        $controller = 'App\Http\Controllers\ProgressTrackingController';
        $progressController = app($controller);
	    $progressController->markCompleted(1);
        $this->seeInDatabase('progress_trackings',['user_id' => $this->user->id, 'module_id' => 1,'completed' => true]);
    }

    /** @test */
    public function if_the_user_can_see_progress()
    {
        $controller = 'App\Http\Controllers\ProgressTrackingController';
        $progressController = app($controller);
        $progressController->markCompleted(1);
        $progressController->markCompleted(2);
        $progressController->markCompleted(3);
        $progressController->markCompleted(4);
        $this->assertTrue($progressController->checkCompleted(1) == true);
        $this->assertTrue($progressController->checkCompleted(2) == true);
        $this->assertTrue($progressController->checkCompleted(3) == true);
        $this->assertTrue($progressController->checkCompleted(4) == true);
        $this->assertTrue($progressController->checkCompleted(5) == false);
    }

    /** @test */
    public function if_the_user_can_see_module_progress()
    {
        $controller = 'App\Http\Controllers\ProgressTrackingController';
        $progressController = app($controller);
	    $progressController->markCompleted(5);
        $progress = $progressController->checkCompleted(5);
        $this->assertTrue($progress);
        $progress = $progressController->checkCompleted(1000);
        $this->assertFalse($progress);
    }
}
