@extends('layouts.master')

@section('title', 'Inicio')
@section('menu')
<li><a href="#intro">Acerca</a></li>
<li><a href="#work">Contenido</a></li>
<li><a href="#contact">Contacto</a></li>
@endsection
@section('drops')
<li><a href="#intro">Acerca</a></li>
<li><a href="#work">Contenido</a></li>
<li><a href="#contact">Contacto</a></li>
@endsection
@section('content')
<!--Hero-->
<div class="section" id="index-banner" style="height:94vh">
    <div class="container">
        <h1 class="text_h center header cd-headline letters type">
        <span>Yo</span>
        <span class="cd-words-wrapper waiting">
            <b class="is-visible">Construyo</b>
            <b>Emprendo</b>
        </span>
        </h1>
            <div class="row">
                <div class="col s12 m4 l4">
                    <img class="responsive-img" src="img/CEMEX_BLANCO.png">
                </div>
                <div class="col s12 m4 l4">
                    <img class="responsive-img" src="img/Centro_Blanco.png">
                </div>
                <div class="col s12 m4 l4">
                    <img class="responsive-img" src="img/Logo_TEC_Blanco.png">
                </div>
            </div>
    </div>
</div>
<!--Intro and service-->
<div id="intro" class="section scrollspy">
    <div class="container">
        <div class="row">
            <div  class="col s12">
                <h2 class="center header text_b">Yo Construyo</h2>
                <h2 class="center header text_b" style="font-size: 30px">Manual de Autoconstrucción</h2>
                <h2 class="center header text_h2">
                        CEMEX y el Tecnológico de Monterrey desarrollaron este manual de autoconstrucción en el que, paso a paso y de manera sencilla, se presenta el proceso constructivo completo para llegar a tener una casa terminada y realizada con criterios sostenibles.
                </h2>
            </div><br/><br/><br/>
            <div class="col s12">
                <div class="col s4">
                    <img class="responsive-img" src="img/CEMEX.png">
                </div>
                <div class="col s4">
                    <img class="responsive-img" src="img/LOGO Centro CX-TEC.png">
                </div>
                <div class="col s4">
                    <img class="responsive-img" src="img/LogoITESM.png">
                </div>
            </div>
        </div>
    </div>
</div>
<!--Content-->
<div class="section scrollspy" id="work">
    <div class="container">
        <h2 class="header text_b">Contenido</h2>
        <div class="row">
            @foreach($modules as $module)
                @include('modules._module')
            @endforeach
        </div>
    </div>
</div>
@endsection