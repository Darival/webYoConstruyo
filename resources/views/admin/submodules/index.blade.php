@extends('layouts.app')

@section('content')

    <h1>SubModules <a href="{{ url('/admin/submodules/create') }}" class="btn btn-primary pull-right btn-sm">Add New SubModule</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Index</th><th>Title</th><th>Module</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach($submodules as $item)
                <tr>
                    <td>{{ $item->index }}</td>
                    <td><a href="{{ url(route('submodule.show',[$item->module->id,$item->id])) }}">{{ $item->title }}</a></td>
                    <td><a href="{{ url(route('module.show',[$item->module->id])) }}">{{ $item->module->title }}</a></td>
                    <td>
                        <a href="{{ url(route('submodule.show',[$item->module->index,$item->index])) }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/submodules', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
