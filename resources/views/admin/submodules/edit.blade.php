@extends('layouts.app')

@section('content')

    <h1>Edit Module</h1>
    <hr/>

    {!! Form::model($subModule, [
        'method' => 'PATCH',
        'url' => ['/admin/submodules', $subModule->id],
        'class' => 'form-horizontal'
    ]) !!}

    <div class="form-group {{ $errors->has('index') ? 'has-error' : ''}}">
        {!! Form::label('index', 'Index: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::number('index', 'value', ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('index', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    

    <div class="form-group {{ $errors->has('module_id') ? 'has-error' : ''}}">
        {!! Form::label('module_id', 'Module Parent: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-6">
            {!!Form::select('module_id', $modules, null, ['placeholder' => 'Select Parent Module', 'required' => 'required'])!!}
            {!! $errors->first('module_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Edit', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection