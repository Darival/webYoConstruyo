@extends('layouts.app')

@section('content')

    <h1>Modules <a href="{{ url('/admin/modules/create') }}" class="btn btn-primary pull-right btn-sm">Add New Module</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Index</th><th>Title</th><th>Thumbnail</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($modules as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url(route('admin.submodules.index')).'?module_id='.$item->id }}">{{ $item->title }}</a></td>
                    <td><img style="width:30%" src="{{ url($item->thumbnail) }}"></td>
                    <td>
                        <a href="{{ url('/admin/modules/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['/admin/modules', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
