<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <meta name="theme-color" content="#2196F3">
        <title>Yo Construyo @yield('title')</title>
        <!-- CSS  -->
        {{ HTML::style('css/animate.css') }}
        {{ HTML::style('min/plugin-min.css') }}
        {{ HTML::style('min/custom-min.css') }}
        {{ HTML::style('js/lity-1.6.6/dist/lity.min.css') }}

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        @stack('styles')
    </head>
    <body id="top" class="scrollspy" style="background: #EEEEEE">
        <!-- Pre Loader -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        
        <!--Navigation-->
        <div class="navbar-fixed">
            <nav id="nav_f" class="default_color" role="navigation">
                <div class="container" style="min-width: 95%;">
                    <div class="nav-wrapper">
                        <div class="row">
                            <div class="col s2">
                                <a href="{{route('home')}}" id="logo-container" class="brand-logo">
                                    <div style="float: left; width: 20%;">
                                        <img class="activator" src="{{url('img/Yo Construyo.png')}}" style="max-width: 90%;max-height: 55px">
                                    </div>
                                    <div>Yo <strong>Construyo</strong></div> 
                                </a>
                            </div>
                            <div class="col s8 offset-s3">
                                <ul class="right hide-on-med-and-down">
                                    @yield('menu')
                                    <ul id="dropdown1" class="dropdown-content">
                                        @yield('drops')
                                    </ul>
                                    @if (Auth::check() and Auth::user()->hasRole('Admin'))
                                        <li><a href="{{url('admin')}}">Admin</a></li>
                                    @endif
                                    @unless (Auth::check())
                                        <li><a href="login">Iniciar sesión</a></li>
                                        <li><a href="register">Registro</a></li>
                                    @endunless
                                    @if (Auth::check())
                                        <li><a href="{{url(route('module.index'))}}">Módulos</a></li>
                                        <li><a href="{{url('logout')}}">Salir</a></li> 
                                    @endif
                                </ul>
                                <ul id="nav-mobile" class="side-nav">
                                    @yield('drops')
                                    @unless (Auth::check())
                                    <li><a href="login">Iniciar sesión</a></li>
                                    <li><a href="register">Registro</a></li>
                                    @endunless
                                    @if (Auth::check() and Auth::user()->hasRole('Admin'))
                                        <li><a href="{{url('admin')}}">Admin</a></li>
                                    @endif
                                    @if (Auth::check())
                                        <li><a href="{{url(route('module.index'))}}">Módulos</a></li>
                                        <li><a href="{{url('logout')}}">Salir</a></li> 
                                    @endif
                                </ul>
                                <a href="#" data-activates="nav-mobile" class="button-collapse" style="left: 8px;position: absolute;"><i class="mdi-navigation-menu col s1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <!--Content-->
        @yield('content')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
        {{ HTML::script('min/plugin-min.js') }}
        {{ HTML::script('min/custom-min.js') }}
        {{ HTML::script('js/lity-1.6.6/dist/lity.min.js') }}
        @yield('scripts')
    </body>
</html>