<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <meta name="theme-color" content="#2196F3">
        <title>Yo Construyo @yield('title')</title>
        <!-- CSS  -->
        {{ HTML::style('css/animate.css') }}
        {{ HTML::style('min/plugin-min.css') }}
        {{ HTML::style('min/custom-min.css') }}

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        @stack('styles')
    </head>
    <body id="top" class="scrollspy" style="background: #EEEEEE">
        <!-- Pre Loader -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        
        <!--Navigation-->
        <div class="navbar-fixed">
            <nav id="nav_f" class="default_color" role="navigation">
                <div class="container" style="min-width: 95%;">
                    <div class="nav-wrapper">
                        <div class="row">
                            <div class="col s2">
                                <a href="{{route('home')}}" id="logo-container" class="brand-logo">
                                    <div style="float: left; width: 20%;">
                                        <img class="activator" src="{{url('img/Yo Construyo.png')}}" style="max-width: 90%;max-height: 55px">
                                    </div>
                                    <div>Yo <strong>Construyo</strong></div> 
                                </a>
                            </div>
                            <div class="col s8 offset-s3">
                                <ul class="right hide-on-med-and-down">
                                    @yield('menu')
                                    <ul id="dropdown1" class="dropdown-content">
                                        @yield('drops')
                                    </ul>
                                    @if (Auth::check() and Auth::user()->hasRole('Admin'))
                                        <li><a href="{{url('admin')}}">Admin</a></li>
                                    @endif
                                    @if (Auth::check())
                                        <li><a href="">{{Auth::user()->name}}</a></li>
                                    @endif
                                    @unless (Auth::check())
                                        <li><a href="login">Iniciar sesión</a></li>
                                        <li><a href="register">Registro</a></li>
                                    @endunless
                                    @if (Auth::check())
                                        <li><a href="{{url(route('module.index'))}}">Módulos</a></li>
                                        <li><a href="{{url('logout')}}">Salir</a></li> 
                                    @endif
                                </ul>
                                <ul id="nav-mobile" class="side-nav">
                                    @yield('drops')
                                    @unless (Auth::check())
                                    <li><a href="login">Iniciar sesión</a></li>
                                    <li><a href="register">Registro</a></li>
                                    @endunless
                                    @if (Auth::check() and Auth::user()->hasRole('Admin'))
                                        <li><a href="{{url('admin')}}">Admin</a></li>
                                    @endif
                                    @if (Auth::check())
                                        <li><a href="{{url(route('module.index'))}}">Módulos</a></li>
                                        <li><a href="{{url('logout')}}">Salir</a></li> 
                                    @endif
                                </ul>
                                <a href="#" data-activates="nav-mobile" class="button-collapse" style="left: 8px;position: absolute;"><i class="mdi-navigation-menu col s1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <!--Content-->
        @yield('content')
        <!--Parallax-->
        <div class="parallax-container">
            <div class="parallax"><img src="{{url('img/parallax1.png')}}"></div>
        </div>
        <!--Footer-->
        <footer id="contact" class="page-footer default_color scrollspy" style="height: 100%;">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        {!! Form::open(['url' => 'message','class' => 'col s12']) !!}
                            <div class="row">
                                <div class="input-field col s6">
                                    <i class="mdi-action-account-circle prefix white-text"></i>
                                    <input id="icon_prefix" name="name" type="text" class="validate white-text">
                                    <label for="icon_prefix" class="white-text">Nombre</label>
                                </div>
                                <div class="input-field col s6">
                                    <i class="mdi-communication-email prefix white-text"></i>
                                    <input id="icon_email" name="email" type="email" class="validate white-text">
                                    <label for="icon_email" class="white-text">Email-id</label>
                                </div>
                                <div class="input-field col s12">
                                    <i class="mdi-editor-mode-edit prefix white-text"></i>
                                    <textarea id="icon_prefix2" name="content" class="materialize-textarea white-text"></textarea>
                                    <label for="icon_prefix2" class="white-text">Mensaje</label>
                                </div>
                                <div class="col offset-s7 s5">
                                    <button class="btn waves-effect waves-light red darken-1" type="submit">
                                        Enviar
                                        <i class="mdi-content-send right white-text"></i>
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col l3 s12">
                        <h5 class="white-text">Social</h5>
                        <ul>
                            <li>
                                <a class="white-text" href="https://twitter.com/centrocemextec">
                                    <i class="small fa fa-twitter-square white-text"></i> Twitter
                                </a>
                            </li>
                            <li>
                                <a class="white-text" href="https://www.facebook.com/centrocemextec">
                                    <i class="small fa fa-facebook-square white-text"></i> Facebook
                                </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!--  Scripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
        {{ HTML::script('min/plugin-min.js') }}
        {{ HTML::script('min/custom-min.js') }}
        @yield('scripts')
    </body>
</html>