<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <meta name="theme-color" content="#2196F3">
        <title>Yo Construyo @yield('title')</title>
        <!-- CSS  -->
        {{ HTML::style('css/animate.css') }}
        {{ HTML::style('min/plugin-min.css') }}
        {{ HTML::style('min/custom-min.css') }}
        {{ HTML::style('js/lity-1.6.6/dist/lity.min.css') }}

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        @stack('styles')
    </head>
    <body id="top" class="scrollspy" style="background: #EEEEEE">
        <!-- Pre Loader -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <!--Content-->
        @yield('content')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
        {{ HTML::script('min/plugin-min.js') }}
        {{ HTML::script('min/custom-min.js') }}
        {{ HTML::script('js/lity-1.6.6/dist/lity.min.js') }}
        @yield('scripts')
    </body>
</html>