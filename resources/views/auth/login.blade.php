@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <h2 class="header" style="color: #3A89C9; font-weight: 300;">Login</h2>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-field col-md-6">
                                <input type="email" class="validate" name="email" value="{{ old('email') }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-field col-md-6">
                                <input type="password" class="form-control" name="password">
                                <label class="col-md-4 control-label">Password</label>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <p>
                                <input type="checkbox" id="test5" name="remember"/>
                                <label for="test5">Recordar</label>
                            </p>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Entrar
                                    <i class="fa fa-btn fa-sign-in"></i>
                                </button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">¿Olvidaste tu Constraseña?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection