@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <h2 class="header" style="color: #3A89C9; font-weight: 300;">Registro</h2>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="row">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="input-field col s6">
                                    <input id="name" type="text" class="validate" name="name" value="{{ old('name') }}">
                                        <label for="name">Nombre</label>
                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <div class="input-field col s6">
                                        <input id="last_name" type="text" class="validate" name="last_name" value="{{ old('last_name') }}">
                                        <label for="last_name">Apellido</label>
                                        @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group{{ $errors->has('gender') ? ' has-error' : '' }}" style="padding-left: 1rem">
                                <div class="col s12 m6 l6">
                                    <label style="color: #9e9e9e; font-size: 1rem">Genero:</label>
                                    <p>
                                        <input name="gender" type="radio" id="male" value="{{ old('gender')? old('gender'):'male' }}"/>
                                        <label for="male">Masculino</label>
                                    </p>
                                    <p>
                                        <input name="gender" type="radio" id="female" value="{{ old('gender')? old('gender'):'female' }}"/>
                                        <label for="female">Femenino</label>
                                    </p>
                                </div>
                                <div class="col s12 m6 l6">
                                    <label style="color: #9e9e9e; font-size: 1rem">Fecha de nacimiento:</label>
                                    <input id="dob" name="dob" type="date" class="datepicker">
                                </div>
                            </div>
                            <div class="row form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-field col s12">
                                    <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                                    <label for="email">Email</label>
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="input-field col s12">
                                    <input id="password" type="password" class="validate" name="password">
                                    <label for="password">Contraseña</label>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="input-field col s12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" class="validate" name="password_confirmation">
                                    <label for="password">Confirmar contraseña</label>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <button class="btn waves-effect waves-light" type="submit">Registrar <i class="large material-icons">input</i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 100, // Creates a dropdown of 15 years to control year,
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdaysShort: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
        today: 'Hóy',
        clear: 'limpiar',
        close: 'Aceptar',
        formatSubmit: 'yyyy/mm/dd',
        format: 'mmmm d, yyyy',
    });
</script>
@endsection