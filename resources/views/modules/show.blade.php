@extends('layouts.master')
@section('title', $module->title)
@section('content')
<div class="container">
    <div class="collection" style="background-color: white">
        <a class="collection-header"><h4  style="padding-left:5px">{{$module->title}}</h4></a>
        <div class="divider"></div>
        @foreach($subModules as $submodule)
            <a href="{{url(route('submodule.show',[$module->id,$submodule['id']]))}}" class="collection-item">
                <i class="material-icons tiny">send</i>&nbsp;{{$submodule['title']}}
                @if($submodule['status'])
                    <i class="material-icons right">check</i>
                @endif
            </a>
        @endforeach
    </div>
</div>
@endsection
