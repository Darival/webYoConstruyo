<div class="col s12 m6 l4">
    <div class="card hoverable">
        <a href="{{route('module.show',[$module['index']])}}">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="{{url($module['thumbnail'])}}">
            </div>
            <div class="card-content truncate">
                {{$module['title']}}
            </div>
            <span class="Card__difficulty"> Módulo {{$module['index']}}</span>
            @if($module['completed'])
                <i class="medium material-icons Card__status">done</i>
            @endif
        </a>
    </div>
</div>