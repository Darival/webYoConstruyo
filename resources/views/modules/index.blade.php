@extends('layouts.master')
@section('title', 'Progreso')
@section('content')
<div class="section scrollspy" id="work">
    <div class="container">
        <h2 class="header text_b">Contenido</h2>
        <div class="row">
            @foreach($modules as $module)
                @include('modules._module')
            @endforeach
        </div>
    </div>
</div>
@endsection