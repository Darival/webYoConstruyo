@if($previousSubModule)
    <a href="{{url(route('submodule.show',[$module->id,$previousSubModule->id]))}}" class="btn-floating btn-large waves-effect waves-light" style="top: 40%;left:10px; position: fixed"><i class="material-icons">keyboard_arrow_left
    </i></a>
@endif
@if($nextSubModule)
    <a href="{{url(route('submodule.show',[$module->id,$nextSubModule->id]))}}" class="btn-floating btn-large waves-effect waves-light" style="top: 40%;right:10px; position: fixed"><i class="material-icons">keyboard_arrow_right
    </i></a>
@elseIf($nextModule)
    <a href="{{url(route('module.show',$nextModule->index))}}" class="red btn-floating btn-large waves-effect waves-light" style="top: 40%;right:10px; position: fixed"><i class="material-icons">keyboard_arrow_right
    </i></a>
@endIf