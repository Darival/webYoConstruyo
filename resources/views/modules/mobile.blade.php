@extends('layouts.mobile')
@section('title', $module->title)
@push('styles')
<style>
    .selected {
        background-color: #ccc !important;
    }
    .hover{
        background-color: #e7ffff;
    }
    .description {
        border-color: #3A89C9!important;
        background-color: #e7ffff!important;
        padding: 0.01em 16px;
        border-left: 6px solid #ccc!important;
        margin-bottom: 1em;
    }
    .card .card-title {
        color: #fff;
        font-size: 35px;
        font-weight: 3.5rem;
    }
    .card-content ul{
        list-style-type: inherit;
        padding-left: 40px;
    }
    .card-content ul li{
        list-style-type: inherit;
    }
    h1{
        color: #3A89C9;
        font-weight: 30px;
        font-size: 3rem;
    }
    h2{
        color: #3A89C9;
        font-weight: 300;
        font-size: 2.5rem;
    }
    h3{
        color: #3A89C9;
        font-weight: 300;
        font-size: 2.0rem;
    }
    h4{
        color: #3A89C9;
        font-weight: 300;
        font-size: 1.5rem;
    }
    h5{
        color: #3A89C9;
        font-weight: 300;
        font-size: 1.0rem;
    }
    h6{
        color: #3A89C9;
        font-weight: 300;
        font-size: .5rem;
    }
</style>
@endpush
@section('menu')
<li><a class="dropdown-button" href="#!" data-beloworigin="true" data-activates="dropdown1">{{ $module->title }}<i class="material-icons right">keyboard_arrow_down</i></a></li>
@endsection
@section('drops')
@foreach($module->subModules as $sub)
<li><a class="truncate" href="{{url(route('submodule.show',[$module->id,$sub->id]))}}">{{$sub->title}}</a></li>
<li class="divider"></li>
@endforeach
@endsection
@section('content')
<dir id="{{str_replace(' ','_',$submodule->title)}}" class="section scrollspy" style="position:relative; padding:0;margin:0">
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="card-content">
                    <div class="card-title" style="color: #2196F3">{{$submodule->title}}</div>
                    <div data-name="content">
                        {!!$submodule->content!!}
                        {!!$submodule->js!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</dir>
@endsection
@section('scripts')
<script>
    Vue.transition('fade',{
        enterClass: 'fadeIn',
        leaveClass: 'fadeOut'
    });


    // register
    Vue.component('r-component', {
    template: '<div class="animated"><h4 style="color: #2196F3">CEMENTO PORTLAND COMPUESTO 30R</h4><p>Alta resistencia inicial, se pueden levantar muros, zapatas, castillos, columnas, trabes, dalas, losas, pisos, pavimentos, banquetas, escaleras, etc.</p></div>'
    })
    Vue.component('rb-component', {
    template: '<div class="animated"><h4 style="color: #2196F3">CEMENTO PORTLAND COMPUESTO 30RB</h4><p>Mismas propiedades del 30R pero puede utilizarse en fachadas, ya que está libre de óxido de fierro. Se caracteriza de mantenimiento sencillo y económico.</p></div>'
    })
    Vue.component('mortero-component', {
    template: '<div class="animated"><h4 style="color: #2196F3">CEMENTO MORTERO</h4><p>Se utiliza para los recubrimientos, para las juntas o para cualquier trabajo de albañilería. Sus propiedades no están diseñadas para trabajarse en sistemas estructurales, podría causar accidentes graves.</p></div>'
    })
    Vue.component('impercem-component', {
    template: '<div class="animated"><h4 style="color: #2196F3">CEMENTO IMPERCEM</h4><p>Esta fabricado con base en el Cemento Extra. No permite el paso de la humedad, evitando que el agua se filtre o se transmine durante la vida útil del concreto.</p></div>'
    })
    Vue.component('multiplast-component', {
    template: '<div class="animated"><h4 style="color: #2196F3">CEMENTO MULTIPLAST</h4><p>Elaborado a base de cemento blanco, agregados seleccionados y aditivos especiales, que al mezclarse con agua forman una pasta de fácil colocación y gran durabilidad. Se utiliza como sustituto del yeso y morteros. Permite hacer trabajos de recubrimiento en interiores y exteriores, con diversidad de texturas y acabados.</p></div>'
    })

    if($('#concreto').length){
        new Vue({
            el: '#concreto',
            data: {
                currentView: 'r-component',
                views:[
                    'r-component',
                    'rb-component',
                    'mortero-component',
                    'impercem-component',
                    'multiplast-component'
                ],
            },
            ready: function(){
            },
            methods: {
                mouseOver: function(id){
                    $('#costal_'+id).addClass('hover');
                },
                showComponent: function(id){
                    for(var i=0;i<5;i++){
                        if(i==id){
                            $('#costal_'+i).addClass('selected');
                            console.log(i+' selected');
                        }else{
                            $('#costal_'+i).removeClass('selected');
                            console.log(i+' removed');
                        }
                    }
                    this.currentView = this.views[id];
                },
                mouseOut: function(id){
                    $('#costal_'+id).removeClass('hover');
                }
            }
        });
    }
    if($('#evo').length){
        new Vue({
            el: '#evo',
            data: {
                index: 1,
            },
            ready: function(){
                this.timer = setInterval(this.toggleImg, 2000);
            },
            methods: {
                toggleImg:function(){
                    if(this.index>3){
                        this.index = 1;
                    }else{
                        this.index++;
                    }
                },
                show: function(id){
                    if(id == this.index){
                        return true;
                    }
                    return false;
                }
            }
        });
    }
</script> 

@if (session('status'))
    <script>
        Materialize.toast('{{ session('status') }}', 5000)
    </script>
@endif
@endsection