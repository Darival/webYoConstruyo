<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Question extends Model
{
    use CrudTrait;
    
    protected $table = 'questions';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	// protected $guarded = ['id'];
	protected $fillable = ['type', 'question','options','correct_answer'. 'homework_id'];
	protected $hidden = ['id'];
    // protected $dates = [];

    public function homework()
    {
        return $this->belongsTo('App\Module\Homework');
    }

    public function progress_tracks()
    {
        return $this->morphMany('App\Models\ProgressTracking','progress_trackeable');
    }
}
