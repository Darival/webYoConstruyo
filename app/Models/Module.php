<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Module extends Model
{
    use CrudTrait;

    protected $table = 'modules';

    protected $fillable = [
        'title', 'index', 'thumbnail', 'public'
    ];

    public function subModules()
    {
        return $this->hasMany('App\Models\SubModule','module_id');
    }

    public function homeworks()
    {
        return $this->hasMany('App\Models\Homework');
    }
}