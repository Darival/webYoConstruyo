<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class SubModule extends Model
{
    use CrudTrait;

    protected $table = 'sub_modules';

    protected $fillable = [
        'content', 'title', 'index', 'module_id', 'js', 'thumbnail'
    ];

    public function module()
    {
        return $this->belongsTo('App\Models\Module','module_id');
    }

    public function progress_tracks()
    {
        return $this->morphMany('App\Models\ProgressTracking','progress_trackeable');
    }
}
