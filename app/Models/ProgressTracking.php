<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgressTracking extends Model
{
    protected $fillable = [
        'completed','user_id', 'progress_trackeable_type', 'progress_trackeable_id'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function progress_trackeable()
    {
        return $this->morphTo();
    }
}
