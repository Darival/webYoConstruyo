<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Activity extends Model
{
    use CrudTrait;

    protected $table = 'activities';

    protected $fillable = [
        'view','sub_module_id'
    ];

    public function subModule()
    {
        return $this->belongsTo('App\Models\SubModule','sub_module_id');
    }
}
