<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Homework extends Model
{
    use CrudTrait;
    
    protected $table = 'homeworks';
	protected $primaryKey = 'id';
	// public $timestamps = false;
	// protected $guarded = ['id'];
	protected $fillable = ['module_id'];
	protected $hidden = ['id'];
    // protected $dates = [];

    public function module()
    {
        return $this->belongsTo('App\Models\Module');
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }
}
