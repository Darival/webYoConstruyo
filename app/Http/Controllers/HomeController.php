<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Module;
use Auth;
use App\Http\Controllers\ProgressTrackingController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProgressTrackingController $progress)
    {
        $this->progress = $progress;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::where('public',true)->get();
        $progress = collect([]);
        if (Auth::check()) {
            $progress = $this->progress->progress()->sortBy(function ($product, $key) {
                return $product['module_id'];
            });
        }
        return view('welcome',compact('modules','progress'));
    }
}