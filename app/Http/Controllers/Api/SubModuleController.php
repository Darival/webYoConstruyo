<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Models\Module;
use App\Http\Controllers\Controller;
use App\Models\SubModule;

use App\Http\Requests;
use JWTAuth;

class SubModuleController extends Controller
{

    public function __construct(){
        $this->middleware('jwt.auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($moduleId)
    {
        return SubModule::where('module_id',$moduleId)->get()
            ->map(function($submodule){
            return [
                'title' => $submodule->title,
                'index'     => $submodule->index,
                'id'    => $submodule->id,
                'thumbnail' => $submodule->thumbnail,
                'completed' => $this->checkIfCompleted($submodule)
            ];
        });
    }

    public function checkIfCompleted($submodule)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $status = $submodule->progress_tracks()
                    ->whereUserId($user->id)->first()?true:false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module, SubModule $submodule, Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
