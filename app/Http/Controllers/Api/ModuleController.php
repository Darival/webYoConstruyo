<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Module;

use App\Http\Requests;
use JWTAuth;

class ModuleController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Module::where('public',true)->get()->map(function($module){
            return [
                'title' => $module->title,
                'index'     => $module->index,
                'id'    => $module->id,
                'thumbnail' => $module->thumbnail,
                'visible' => $module->public,
                'completed' => $this->checkIfCompleted($module)
            ];
        });
    }

    public function checkIfCompleted($module)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $status = $module->subModules
        ->reject(function($submodule) use (&$user){
            return $submodule->progress_tracks()->whereUserId($user->id)
                ->first();
        })->count()?false:true;
        return $status;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
