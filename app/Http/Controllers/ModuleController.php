<?php

namespace App\Http\Controllers;


use App\Http\Requests;

use App\Models\Module;
use App\Models\SubModule;

use Auth;

class ModuleController extends Controller
{

    public function show(Module $module)
    {
        
        $subModules = $module->subModules->map(function($subModule){
            $status = $subModule->progress_tracks()
                        ->whereUserId(Auth::user()->id)->first()?true:false;
            return [
                'id' => $subModule->id,
                'title' => $subModule->title,
                'status' => $status,
            ];
        });
        return view('modules.show', compact('module', 'subModules'));
    }

    public function index()
    {
        $modules = Module::where('public',true)->get();
        $user = Auth::user();
        $modules = $modules->map(function($module) use (&$user){
            $completed = $module->subModules
            ->reject(function($submodule) use (&$user){
                return $submodule->progress_tracks()->whereUserId($user->id)
                    ->first();
            });

            $submodules = $module->submodules->count();
            return [
                'index'    => $module->index,
                'title'     => $module->title,
                'thumbnail' => $module->thumbnail,
                'completed' => ($completed->count())?false:true
            ];
        });
        return view('modules.index',compact('modules'));
    }
}
