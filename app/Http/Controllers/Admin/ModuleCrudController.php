<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;

class ModuleCrudController extends CrudController
{
    public function __construct() {
        parent::__construct();

        $this->crud->setModel("App\Models\Module");
        $this->crud->setRoute("admin/module");
        $this->crud->setEntityNameStrings('module', 'modules');

        $this->crud->setColumns([
            [
                'name'  => 'index',
                'label' => 'Index'
            ],
            [
                'name'  => 'title',
                'label' => 'Title'
            ],
            [
                'name'  => 'public',
                'label' => 'Public',
                'type' => 'check'
            ],
        ]);
        $this->crud->addFields([
            [
                'name' => 'index',
                'label' => 'Index',
                'type' => 'number'
            ],
            [
                'name'  => 'title',
                'label' => 'Title'
            ],
            [
                'name'  => 'thumbnail',
                'label' => 'Thumbnail',
                'type'  => 'browse'
            ],
            [
                'name'  => 'public',
                'label' => 'Public',
                'type' => 'checkbox'
            ],
        ]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
