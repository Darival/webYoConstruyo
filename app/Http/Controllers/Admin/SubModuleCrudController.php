<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;

class SubModuleCrudController extends CrudController
{
    public function __construct() {
        parent::__construct();

        $this->crud->setModel("App\Models\SubModule");
        $this->crud->setRoute("admin/submodule");
        $this->crud->setEntityNameStrings('submodule', 'submodules');

        $this->crud->setColumns([ //'content', 'title', 'index', 'module_id'
            [
                'name'  => 'index',
                'label' => 'Index'
            ],
            [
                'name'  => 'title',
                'label' => 'Title'
            ],
            [
                'label' => "Module", // Table column heading
                'type' => "select",
                'name' => 'module_id', // the method that defines the relationship in your Model
                'entity' => 'module', // the method that defines the relationship in your Model
                'attribute' => "title", // foreign key attribute that is shown to user
                'model' => "App\Models\Module", // foreign key model
            ],
        ]);
        $this->crud->addFields([
            [
                'label' => "Module", // Table column heading
                'type' => "select2",
                'name' => 'module_id', // the method that defines the relationship in your Model
                'entity' => 'module', // the method that defines the relationship in your Model
                'attribute' => "title", // foreign key attribute that is shown to user
                'model' => "App\Models\Module", // foreign key model
            ],
            [
                'name'  => 'title',
                'label' => 'Title'
            ],
            [
                'name' => 'index',
                'label' => "Index",
                'type' => 'number'
            ],
            [
                'name'  => 'thumbnail',
                'label' => 'Thumbnail',
                'type'  => 'browse'
            ],
            [
                'name' => 'content',
                'label' => 'Content',
                'type' => 'customtinymce'
            ],
            [
                'name' => 'js',
                'label' => 'Vue Component',
                'type' => 'textarea'
            ],
        ]);
    }

    public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
