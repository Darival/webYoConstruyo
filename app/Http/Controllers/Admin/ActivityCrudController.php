<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Illuminate\Http\Request as StoreRequest;
use Illuminate\Http\Request as UpdateRequest;

class ActivityCrudController extends CrudController
{
    public function __construct() {
        parent::__construct();

        $this->crud->setModel("App\Models\Activity");
        $this->crud->setRoute("admin/activity");
        $this->crud->setEntityNameStrings('activity', 'activities');

        $this->crud->setColumns([ //'content', 'title', 'index', 'module_id'
            [
                'name'  => 'view',
                'label' => 'View'
            ],
            [
                'label' => "SubModule", // Table column heading
                'type' => "select",
                'name' => 'sub_module_id', // the method that defines the relationship in your Model
                'entity' => 'submodule', // the method that defines the relationship in your Model
                'attribute' => "title", // foreign key attribute that is shown to user
                'model' => "App\Models\SubModule", // foreign key model
            ],
        ]);
        $this->crud->addFields([
            [
                'label' => "SubModule", // Table column heading
                'type' => "select",
                'name' => 'sub_module_id', // the method that defines the relationship in your Model
                'entity' => 'submodule', // the method that defines the relationship in your Model
                'attribute' => "title", // foreign key attribute that is shown to user
                'model' => "App\Models\SubModule", // foreign key model
            ],
            [
                'name'  => 'view',
                'label' => 'View'
            ],
        ]);
    }

    public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
