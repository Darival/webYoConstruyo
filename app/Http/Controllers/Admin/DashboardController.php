<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Message;

class DashboardController extends Controller
{
    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $data = ['title' => trans('backpack::base.dashboard')]; // set the page title

        $registeredUsers = User::count();

        $messages = Message::count();

        return view('backpack::dashboard',compact(['data','registeredUsers','messages']));
    }
}
