<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\ProgressTracking;
use Illuminate\Http\Response;
use App\Models\Module;

class ProgressTrackingController extends Controller
{	
	
    public function markCompleted($moduleId)
    {
    	$progress = ProgressTracking::firstOrCreate(['user_id' => Auth::user()->id,'module_id' => $moduleId, 'completed' => true]);
		return view('welcome');
    }

    public function progress($moduleId = null){
    	if ($moduleId) {
    		$completed = Auth::user()->progress()->where('completed',true)->where('module_id',$moduleId)->get();
    		if($completed->count()){
    			return true;
    		}
    		return false;
    	}
    	return Auth::user()->progress()->where('completed',true)->get();
    }

}


