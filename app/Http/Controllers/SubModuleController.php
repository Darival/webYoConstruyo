<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Module;
use App\Models\SubModule;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class SubModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module, SubModule $submodule, Request $request)
    {
        $user = Auth::user();
        $module->load('subModules');
        $already = $module->subModules
        ->reject(function($submodule) use (&$user){
            return $submodule->progress_tracks()->whereUserId($user->id)
                ->first();
        })->count()?false:true;
        $user->progress()->firstOrCreate([
            'completed' => true,
            'user_id' => $user->id,
            'progress_trackeable_type' => 'App\Models\SubModule',
            'progress_trackeable_id' => $submodule->id
        ]);
        $completedModule = $module->subModules
        ->reject(function($submodule) use (&$user){
            return $submodule->progress_tracks()->whereUserId($user->id)
                ->first();
        })->count()?false:true;
        if(!$already && $completedModule){
            $request->session()->flash('status', "Haz terminado el modulo \"".$module->title."\"!");
        }
        $previousSubModule = $module->subModules->sortBy('index')->first(function($key,$sub) use($submodule){
            return $sub->index == $submodule->index-1;
        });
        $nextSubModule = $module->subModules->sortBy('index')->first(function($key,$sub) use($submodule){
            return $sub->index == $submodule->index+1;
        });
        $nextModule = Module::whereIndex($module->index + 1)->first();
        return view('modules.submodule', compact('module', 'submodule','previousSubModule','nextSubModule', 'nextModule'));
    }

    public function showMobile(Module $module, SubModule $submodule, Request $request)
    {
        $module->load('subModules');

        return view('modules.mobile', compact('module', 'submodule','previousSubModule','nextSubModule', 'nextModule'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
