<?php

Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/module/index',['as' => 'module.index', 'uses' => 'ModuleController@index']);
    Route::get('/module/{module}',['as' => 'module.show', 'uses' => 'ModuleController@show']);
    Route::get('/module/{module}/submodule/{submodule}',['as' => 'submodule.show', 'uses' => 'SubModuleController@show']);
});

Route::group(['prefix'=>'admin', 'middleware' => ['web','auth','onlyRole'],'roles'=>'Admin'], function () {
    CRUD::resource('module', 'Admin\ModuleCrudController');
    CRUD::resource('submodule', 'Admin\SubModuleCrudController');
    CRUD::resource('activity', 'Admin\ActivityCrudController');
    Route::get('dashboard', 'Admin\DashboardController@dashboard');
    CRUD::resource('permission', 'Admin\PermissionCrudController');
    CRUD::resource('role', 'Admin\RoleCrudController');
    CRUD::resource('user', 'Admin\UserCrudController');
    CRUD::resource('message', 'Admin\MessageCrudController');
    Route::get('/', 'Admin\DashboardController@dashboard');
});

Route::group(['middleware' => ['web']], function(){
    Route::auth();
    Route::get('/',['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('module/{module}/submodule/{submodule}/mobile', 'SubModuleController@showMobile');
    Route::post('message', 'MessageController@store');
});

Route::group(['prefix' => 'api/v1', 'middleware' => ['cors']], function()
{
    Route::post('authenticate', 'AuthenticateController@authenticate');
    Route::post('register', 'Auth\AuthControllerApi@register');
    Route::post('refresh', 'AuthenticateController@refreshToken');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');

    Route::get('modules', 'Api\ModuleController@index');
    Route::get('submodules/{moduleId}', 'Api\SubModuleController@index');
    Route::post('progress', 'Api\ProgressTrackingController@store');
});